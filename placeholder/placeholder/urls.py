"""placeholder URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from views import Home
from submissions import views
from django.contrib.auth import views as django_views #for logout and password stuff, which should probs be in accounts/urls.py but I couldn't get it to work - emily


import comments.views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^$', Home.as_view(), name='home'),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^logout/$', django_views.logout, {'next_page': '/'}),
    url(r'^(?P<id>\d+)/comments/', include('comments.urls')),
    url(r'', include('submissions.urls')),
    url(r'', include('django.contrib.auth.urls')),
    url(r'^accounts/password/reset/$', django_views.password_reset, name='password_reset', 
        kwargs={'template_name': 'accounts/password_reset.html'
        }),
    url(r'^accounts/password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', 
        django_views.password_reset_confirm,  name='password_reset_confirm',
        kwargs={'post_reset_redirect' : '/user/password/done/'}),
    url(r'^accounts/password/reset/complete/$', django_views.password_reset_complete, name='password_reset_complete',
         kwargs={'template_name': 'accounts/password_reset_complete.html' }),
    url(r'^accounts/password/reset/done/$', django_views.password_reset_done, name='password_reset_done',
         kwargs={'template_name': 'accounts/password_reset_done.html' }),
    url(r'^accounts/password/change/$', django_views.password_change, 
        kwargs= {'template_name': 'accounts/password_change.html'})
]
