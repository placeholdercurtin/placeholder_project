from django.contrib import admin

from comments.models import Comment


class CommentAdmin(admin.ModelAdmin):
	list_display = ('author', 'created_at', 'updated_at', 'submission',)
	list_filter = ('created_at', 'updated_at')

	readonly_fields = ('created_at','updated_at',)

	fieldsets = [
		('Comment', {
			'fields': ('text', 'points','submission',)
		}),
		('author', {
			'classes': ('collapse',),
			'fields': ('author',)
		}),
		('Change History', {
			'classes': ('collapse',),
			'fields': ('created_at', 'updated_at')
		})
	]

admin.site.register(Comment, CommentAdmin)