import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, request, HttpResponseRedirect
from django.utils.timezone import utc
from django.conf import settings

from comments.models import Comment
from submissions.models import Submission
from comments.forms import CommentForm
from django.contrib.auth.models import User
from django.http import JsonResponse


def score(comment):
	points = comment.points
	now = datetime.datetime.utcnow().replace(tzinfo=utc) 
	age = int((now - comment.created_at).total_seconds())/60
	return points

def top_comments(submission, top=180, consider=1000):
	allcomments = Comment.objects.all()
	allthreads = Submission.objects.all()
 	submission_thread =  Comment.objects.filter(id__in=[comment.id for comment in allcomments if comment.submission.id == submission.id])
 	latest_comments = submission_thread.order_by('-created_at')[:consider]
 	ranked_comments = sorted([(score(comment), comment) for comment in latest_comments], reverse = True)
 	print [comment.submission for _, comment in ranked_comments][:top]
	return[comment for _, comment in ranked_comments][:top]


def index(request, id):
	thread = get_object_or_404(Submission, pk=id)
	comments = top_comments(thread, top=30)
	url = thread.get_absolute_url
	if request.user.is_authenticated():
		liked_comments = request.user.liked_comments.filter(id__in=[comment.id for comment in comments])
		disliked_comments = request.user.disliked_comments.filter(id__in=[comment.id for comment in comments])
		loved_comments = request.user.loved_comments.filter(id__in=[comment.id for comment in comments])
		hated_comments = request.user.hated_comments.filter(id__in=[comment.id for comment in comments])
	else:
		liked_comments = []
		disliked_comments = []
		loved_comments = []
		hated_comments = []
	return render(request, 'comments/index.html', {
		'comments': comments,
        'thread' : thread,
        'liked_comments': liked_comments,
        'disliked_comments': disliked_comments,
        'loved_comments' : loved_comments,
        'hated_comments' : hated_comments,
        'url' : url,

	})


@login_required
def comment(request, id):
	submission = get_object_or_404(Submission, pk=id)
	if request.method == 'POST':
		form = CommentForm(request.POST)
		if form.is_valid():
			comment = form.save(commit=False)
			comment.author = request.user
			comment.submission = submission
			comment.save()
			return HttpResponseRedirect('/'+str(id) + '/comments/')
	else:
		form = CommentForm()
	return render(request, 'comments/comment.html', {
		'form': form,
		'submission' : submission,
		'url' : id
		})

@login_required
def voteLike(request, id):
	comment = get_object_or_404(Comment, pk=request.POST.get('comment'))
	user = request.user
	if comment in user.disliked_comments.all():
		user.disliked_comments.remove(comment)
		comment.points += 1
		comment.author.userprofile.points += 1
	if comment in user.loved_comments.all():
		user.loved_comments.remove(comment)
		comment.points -= 3
	if comment in user.hated_comments.all():
		user.hated_comments.remove(comment)
		comment.points += 3
	comment.points += 1
	comment.author.userprofile.points += 1
	user.liked_comments.add(comment)
	comment.author.userprofile.save()
	comment.save()
	user.save()
	return HttpResponse()

@login_required
def voteDislike(request, id):
	comment = get_object_or_404(Comment, pk=request.POST.get('comment'))
	user = request.user
	if comment in user.liked_comments.all():
		user.liked_comments.remove(comment)
		comment.points -= 1
		comment.author.userprofile.points -= 1
	if comment in user.loved_comments.all():
		user.loved_comments.remove(comment)
		comment.points -= 3
		comment.author.userprofile.points -= 3
	if comment in user.hated_comments.all():
		user.hated_comments.remove(comment)
		comment.points += 3
		comment.author.userprofile.points += 3
	comment.points -= 1
	comment.author.userprofile.points -= 1
	user.disliked_comments.add(comment)
	comment.author.userprofile.save()
	comment.save()
	user.save()
	return HttpResponse()

@login_required
def voteHate(request, id):
	comment = get_object_or_404(Comment, pk=request.POST.get('comment'))
	user = request.user
	if comment in user.liked_comments.all():
		user.liked_comments.remove(comment)
		comment.points -= 1
		comment.author.userprofile.points -= 1
	if comment in user.loved_comments.all():
		user.loved_comments.remove(comment)
		comment.points -= 3
		comment.author.userprofile.points -= 3
	if comment in user.disliked_comments.all():
		user.disliked_comments.remove(comment)
		comment.points += 1
		comment.author.userprofile.points += 1
	comment.points -= 3
	comment.author.userprofile.points -= 3
	user.hated_comments.add(comment)
	comment.author.userprofile.save()
	comment.save()
	user.save()
	return HttpResponse()

@login_required
def voteLove(request, id):
	comment = get_object_or_404(Comment, pk=request.POST.get('comment'))
	user = request.user
	if comment in user.disliked_comments.all():
		user.disliked_comments.remove(comment)
		comment.points += 1
		comment.author.userprofile.points += 1
	if comment in user.hated_comments.all():
		user.hated_comments.remove(comment)
		comment.points += 3
		comment.author.userprofile.points += 3
	if comment in user.liked_comments.all():
		user.liked_comments.remove(comment)
		comment.points -= 1
		comment.author.userprofile.points -= 1
	comment.points += 3
	comment.author.userprofile.points += 3
	user.loved_comments.add(comment)
	comment.author.userprofile.save()
	comment.save()
	user.save()
	return HttpResponse()


