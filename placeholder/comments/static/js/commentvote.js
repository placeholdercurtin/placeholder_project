$(document).ready(function() {

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function voteLike (commentID) {
    debugger;
    $.ajax({
        type: "POST",
        url: "voteLike/",
        data: {"comment": commentID},
        success: function() {
	    window.location.reload(true);
	    //i commented out the hide arrow thing because refreshing
	    //the page removes the arrow anyway because of the if statements
	    //in the index.html template
            //$("#comment-voteLike-" + commentID).hide();
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
    return false;
}

function voteDislike (commentID) {
    debugger;
    $.ajax({
        type: "POST",
        url: "voteDislike/",
        data: {"comment": commentID},
        success: function() {
	    window.location.reload(true);
            //$("#comment-voteDislike-" + commentID).hide();
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
    return false;
}

function voteLove (commentID) {
    debugger;
    $.ajax({
        type: "POST",
        url: "voteLove/",
        data: {"comment": commentID},
        success: function() {
        window.location.reload(true);
            //$("#comment-voteLike-" + commentID).hide();
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
    return false;
}

function voteHate (commentID) {
    debugger;
    $.ajax({
        type: "POST",
        url: "voteHate/",
        data: {"comment": commentID},
        success: function() {
        window.location.reload(true);
            //$("#comment-voteLike-" + commentID).hide();
        },
        headers: {
            'X-CSRFToken': csrftoken
        }
    });
    return false;
}
                  
$("a.voteLike").click(function() {
    var commentID = parseInt(this.id.split("-")[2]);
    return voteLike(commentID);
});

                 
$("a.voteDislike").click(function() {
    var commentID = parseInt(this.id.split("-")[2]);
    return voteDislike(commentID);
});

$("a.voteLove").click(function() {
    var commentID = parseInt(this.id.split("-")[2]);
    return voteLove(commentID);
});

$("a.voteHate").click(function() {
    var commentID = parseInt(this.id.split("-")[2]);
    return voteHate(commentID);
});
});
