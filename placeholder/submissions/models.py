from __future__ import unicode_literals
from django.core.urlresolvers import reverse
from django.db import models
from urlparse import urlparse
from django.db.models import permalink
#from submissionThread.models import SubmissionThread

from django.contrib.auth.models import User

class Submission(models.Model):
	title = models.CharField(max_length=200)
	url = models.URLField(default='')
	points = models.IntegerField(default=1)
	moderator = models.ForeignKey(User, related_name='moderated_submissions')
	votersLike = models.ManyToManyField(User, related_name='liked_submissions')
	votersDislike = models.ManyToManyField(User, related_name='disliked_submissions')
	votersLove = models.ManyToManyField(User, related_name= 'loved_submissions')
	votersHate = models.ManyToManyField(User, related_name= 'hated_submissions')
	text = models.TextField(null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
#	thread = models.OneToOneField(SubmissionThread)

	@property
	def domain(self):
		return urlparse(self.url).netloc

	def __unicode__(self):
		return (self.title)

	def get_absolute_url(self):
		return "/%s/%s/" % (self.id, 'comments')

