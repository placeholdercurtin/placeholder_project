from django.contrib import admin

from submissions.models import Submission


class SubmissionAdmin(admin.ModelAdmin):
	list_display = ('__unicode__', 'domain', 'moderator', 'created_at', 'updated_at',)
	list_filter = ('created_at', 'updated_at')
	search_fields = ('title', 'moderator__username', 'moderator__first_name','moderator__last_name')
#	def lower_case_title(self, obj):
#		return obj.title.lower()
#	lower_case_title.short_description = 'title'

	#fields = ('title','url','created_at', 'updated_at',)
	readonly_fields = ('created_at','updated_at', 'domain',)

	fieldsets = [
		('Submission', {
			'fields': ('title', 'url', 'points', 'text')
		}),
		('moderator', {
			'classes': ('collapse',),
			'fields': ('moderator',)
		}),
		('Change History', {
			'classes': ('collapse',),
			'fields': ('created_at', 'updated_at')
		})
	]

admin.site.register(Submission, SubmissionAdmin)