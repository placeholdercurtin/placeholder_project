from django.conf.urls import url
import submissions.views
import comments.views

urlpatterns = [
	url(r'^$', submissions.views.index),
#	url(r'^(?P<id>\d+)/comments/$', comments.views.index, name='view_submission_comments'),
	url(r'^submission', submissions.views.submission),
	url(r'^voteLike/$', submissions.views.voteLike),
	url(r'^voteDislike/$', submissions.views.voteDislike),
	url(r'^voteLove/$', submissions.views.voteLove),
	url(r'^voteHate/$', submissions.views.voteHate),
	url(r'^search/$', submissions.views.search),
	url(r'^new/$', submissions.views.sortby_new),
	url(r'^top/$', submissions.views.sortby_top),
]
