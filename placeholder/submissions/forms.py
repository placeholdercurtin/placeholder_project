from django.forms import ModelForm
from submissions.models import Submission
from django.db import models

class SubmissionForm(ModelForm):
	class Meta:
		model = Submission
		fields = ['title', 'url', 'text']

class Submission(object):
	title = models.CharField(max_length=200)
	url = models.URLField(default='')
	text = models.TextField(null=True, blank=True)

#class SubmissionForm(ModelForm):
#    class Meta:
#		model = Submission
#		exclude = ('points', 'moderator',)