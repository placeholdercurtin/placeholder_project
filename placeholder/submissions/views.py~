import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, request, HttpResponseRedirect
from django.utils.timezone import utc
from django.conf import settings

from submissions.models import Submission
from submissions.forms import SubmissionForm
from django.contrib.auth.models import User
from django.db.models import Count
from search_backend import get_query

def score(submission):
	points = submission.points
	now = datetime.datetime.utcnow().replace(tzinfo=utc) 
	age = int((now - submission.created_at).total_seconds())/60
	return points

def top_submissions(top=180, consider=1000):
	latest_submissions = Submission.objects.all().order_by('-created_at')[:consider]
	ranked_submissions = sorted([(score(submission), submission) for submission in latest_submissions], reverse = True)
	return[submission for _, submission in ranked_submissions][:top]

def index(request):
	submissions = top_submissions(top=30)
	if request.user.is_authenticated():
		liked_submissions = request.user.liked_submissions.filter(id__in=[submission.id for submission in submissions])
		disliked_submissions = request.user.disliked_submissions.filter(id__in=[submission.id for submission in submissions])
		loved_submissions = request.user.loved_submissions.filter(id__in=[submission.id for submission in submissions])
		hated_submissions = request.user.hated_submissions.filter(id__in=[submission.id for submission in submissions])
	else:
		liked_submissions = []
		disliked_submissions = []
		loved_submissions = []
		hated_submissions = []
	return render(request, 'submissions/index.html', {
		'submissions': submissions,
		'user': request.user,
		'liked_submissions': liked_submissions,
        'disliked_submissions': disliked_submissions,
        'loved_submissions' : loved_submissions,
        'hated_submissions' : hated_submissions,
	})

@login_required
def submission(request):
	if request.method == 'POST':
		form = SubmissionForm(request.POST)
		if form.is_valid():
			submission = form.save(commit=False)
			submission.moderator = request.user
			submission.save()
			return HttpResponseRedirect('/')
	else:
		form = SubmissionForm()
	return render(request, 'submissions/submission.html', {'form': form})

@login_required
def voteLike(request):
	submission = get_object_or_404(Submission, pk=request.POST.get('submission'))
	user = request.user
	if submission in user.disliked_submissions.all():
		user.disliked_submissions.remove(submission)
		submission.points += 1
	if submission in user.loved_submissions.all():
		user.loved_submissions.remove(submission)
		submission.points -= 3
	if submission in user.hated_submissions.all():
		user.hated_submissions.remove(submission)
		submission.points += 3
	submission.points += 1
	user.liked_submissions.add(submission)
	submission.save()
	user.save()
	return HttpResponse()

@login_required
def voteDislike(request):
	submission = get_object_or_404(Submission, pk=request.POST.get('submission'))
	user = request.user
	if submission in user.liked_submissions.all():
		user.liked_submissions.remove(submission)
		submission.points -= 1
	if submission in user.loved_submissions.all():
		user.loved_submissions.remove(submission)
		submission.points -= 3
	if submission in user.hated_submissions.all():
		user.hated_submissions.remove(submission)
		submission.points += 3
	submission.points -= 1
	user.disliked_submissions.add(submission)
	submission.save()
	user.save()
	return HttpResponse()

@login_required
def voteHate(request):
	submission = get_object_or_404(Submission, pk=request.POST.get('submission'))
	user = request.user
	if submission in user.liked_submissions.all():
		user.liked_submissions.remove(submission)
		submission.points -= 1
	if submission in user.loved_submissions.all():
		user.loved_submissions.remove(submission)
		submission.points -= 3
	if submission in user.disliked_submissions.all():
		user.disliked_submissions.remove(submission)
		submission.points += 1
	submission.points -= 3
	user.hated_submissions.add(submission)
	submission.save()
	user.save()
	return HttpResponse()

@login_required
def voteLove(request):
	submission = get_object_or_404(Submission, pk=request.POST.get('submission'))
	user = request.user
	if submission in user.disliked_submissions.all():
		user.disliked_submissions.remove(submission)
		submission.points += 1
	if submission in user.hated_submissions.all():
		user.hated_submissions.remove(submission)
		submission.points += 3
	if submission in user.liked_submissions.all():
		user.liked_submissions.remove(submission)
		submission.points -= 1
	submission.points += 3
	user.loved_submissions.add(submission)
	submission.save()
	user.save()
	return HttpResponse()

def comments(request, id):
	thread = get_object_or_404(Submission, pk=id)
	return render(request, 'comments/index.html', {
		'thread' : thread,
		})



